package fabrica.desafio;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class NaoCadastrado {

	@Test
	public void CamposVazios() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\jande\\Documents\\Programas para estudo\\drives de browser\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://a.testaddressbook.com/");
		
		driver.findElement(By.xpath("//*[@id=\"navbar\"]/div[1]/a[1]")).click();
		driver.findElement(By.xpath("//*[@id=\"sign-in\"]")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"clearance\"]/div/div/form/div[3]/input")).click();
		String texto = driver.findElement(By.xpath("/html/body/div/div[1]")).getText();
		
		
		Assert.assertEquals("Bad email or password.", texto);
		Thread.sleep(3000);
		driver.quit();
	}
	
	@Test
	public void emailVazio() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\jande\\Documents\\Programas para estudo\\drives de browser\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://a.testaddressbook.com/");
		
		driver.findElement(By.xpath("//*[@id=\"sign-in\"]")).click();
		Thread.sleep(2000);
		driver.findElement(By.id("session_password")).sendKeys("123");
		driver.findElement(By.xpath("//*[@id=\"clearance\"]/div/div/form/div[3]/input")).click();
		
		String texto = driver.findElement(By.xpath("/html/body/div/div[1]")).getText();
		Assert.assertEquals("Bad email or password.", texto);
		
		Thread.sleep(3000);
		driver.quit();
	}
	
	@Test
	public void senhaVazia() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\jande\\Documents\\Programas para estudo\\drives de browser\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://a.testaddressbook.com/");
		
		driver.findElement(By.xpath("//*[@id=\"sign-in\"]")).click();
		Thread.sleep(2000);
		
		driver.findElement(By.id("session_email")).sendKeys("jandeilson@gmail.com");
		driver.findElement(By.xpath("//*[@id=\"clearance\"]/div/div/form/div[3]/input")).click();
		
		String texto = driver.findElement(By.xpath("/html/body/div/div[1]")).getText();
		Assert.assertEquals("Bad email or password.", texto);
		
		Thread.sleep(3000);
		driver.quit();
	}
	
}
