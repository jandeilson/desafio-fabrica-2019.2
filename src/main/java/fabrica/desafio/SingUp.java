package fabrica.desafio;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SingUp {

	@Test
	public void cadastraUsuarioNaoCadastrado() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\jande\\Documents\\Programas para estudo\\drives de browser\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://a.testaddressbook.com/");
		
		driver.findElement(By.xpath("//*[@id=\"sign-in\"]")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"clearance\"]/div/div/form/div[4]/a")).click();
		driver.findElement(By.xpath("//*[@id=\"clearance\"]/div/div/form/div[3]/input")).click();
		
		Thread.sleep(3000);
		driver.quit();
	}
	
	@Test
	public void cadastraUsuario() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\jande\\Documents\\Programas para estudo\\drives de browser\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://a.testaddressbook.com/");
		
		driver.findElement(By.xpath("//*[@id=\"sign-in\"]")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"clearance\"]/div/div/form/div[4]/a")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("user_email")).sendKeys("jandeilson1@gmail.com");
		driver.findElement(By.id("user_password")).sendKeys("123456");
		driver.findElement(By.xpath("//*[@id=\"new_user\"]/div[3]/input")).click();
		String texto = driver.findElement(By.xpath("/html/body/div/div/h1")).getText();
		Thread.sleep(2000);
		Assert.assertEquals("Welcome to Address Book", texto);
		
		
		Thread.sleep(3000);
		driver.quit();
	}
	
	@Test
	public void logarComUsuario() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\jande\\Documents\\Programas para estudo\\drives de browser\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://a.testaddressbook.com/");
		
		driver.findElement(By.xpath("//*[@id=\"sign-in\"]")).click();
		Thread.sleep(2000);
		
		driver.findElement(By.id("session_email")).sendKeys("jandeilson1@gmail.com");
		driver.findElement(By.id("session_password")).sendKeys("123456");
		driver.findElement(By.xpath("//*[@id=\"clearance\"]/div/div/form/div[3]/input")).click();
		
		String texto = driver.findElement(By.xpath("//*[@id=\"navbar\"]/div[2]/span")).getText();
		
		Assert.assertEquals("jandeilson1@gmail.com", texto);
		
		Thread.sleep(3000);
		driver.quit();
	}
}
